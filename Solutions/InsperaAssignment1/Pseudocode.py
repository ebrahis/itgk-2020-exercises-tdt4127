# Part 1
# Check if the number n is a prime number.
# Return true if n is a prime, false else.
def unknown_func(n):
    if n > 1:
        for i in range(2, n//2+1): # '//'is integer division. 6/4=1.5 and 6//4=1
            if n % i == 0:
                return False
    return True

# Part 2
# This code runs forever
# It will first print '###', and then '#', and then '' forever.
# To avoid the while-loop to run forever, x have to be 0 once. 
# 0 is the only number which is considered as False. Everything is counted as True.
x = 3
while x:
    print("#"*x)
    x -= 2

# Part 3
# This function calculates the sum of all the digits in the input number.
# a = 1234 results in r = 10
def unknown_func2(a):
    r = 0
    while a > 0:
        s = a % 10
        r = r + s
        a = (a - s)//10  # Utilize integer division
    return r

print(unknown_func2(1234))
