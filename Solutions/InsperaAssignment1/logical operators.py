# part 1
month = input("Input a month: ")
year = int(input("Input a year: "))
month = month.lower()

if (month == "january") or (month == "mars") or (month == "may") or (month == "july") or (month == "august") or (month == "october") or (month == "december"):
    print(31)
elif (month == "april") or (month == "juny") or (month == "september") or (month == "november"):
    print(30)
elif month == "february":
    if year%4 == 0:
        print(29)
    else:
        print(28)
        
# Part 2
valid = False

while not valid:

    month = input("Input a month: ")
    year = int(input("Input a year: "))
    month = month.lower()
    valid_year = ((year >= 0) and (year <= 2020))
    
    if not valid_year: # Invalid year
        print("Invalid input, try again!")
        continue

    if (month == "january") or (month == "mars") or (month == "may") or (month == "july") or (month == "august") or (month == "october") or (month == "december"):
        number_of_days = 31
        valid = True
    elif (month == "april") or (month == "juny") or (month == "september") or (month == "november"):
        number_of_days = 30
        valid = True
    elif  month == "february":
        if year % 4 == 0:
            number_of_days = 29
        else:
            number_of_days = 28
        valid = True
    else: #invalid month
        print("Invalid month, try again")

print("It is ",number_of_days ,"days in this month.")
