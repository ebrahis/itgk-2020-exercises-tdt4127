l, b, h = 0, 0, 0

def getInputs():
    global l, b, h
    l = float(input("What is the length? "))
    b = float(input("What is the width? "))
    h = float(input("What is the height? "))

getInputs()

while (l == b or l == h or h == b):
    print("Morten is not happy, try again!")
    getInputs()

print("The largest cube will have a volume of:", round(min(l, b, h)**3, 2))
