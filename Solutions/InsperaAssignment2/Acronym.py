# Del 1
def input_strings():
    lst = []
    string = ""
    for i in range(4):
        string = input("Input a string: ")
        lst.append(string)
    return lst


# Del 2
def acronym():
    lst = input_strings()
    for string in lst:
        print(string[0].upper(), end='')
