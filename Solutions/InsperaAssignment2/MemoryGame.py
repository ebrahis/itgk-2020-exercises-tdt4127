# Del 1
def smallify_words(objects):
    new_objects = []
    for word in objects:
        new_objects.append(word.lower())
    return new_objects


# Del 2
def get_five_objects():
    while True:
        objects = (input("Enter five objects separated by ';': ").split(';'))
        if len(objects) == 5:
            return smallify_words(objects)
        print("You were supposed to enter FIVE objects, not", str(len(objects)) + '. Try again.')


# Del 3
def play_game():
    objects = smallify_words(get_five_objects())
    print("""We shall now play a little memory game. Can you remember all the words you gave me?
If it gets to hard for you and you want to give up, write 'quit'""")
    word = input("What is your guess? ").lower()
    while word != 'quit':
        if word in objects:
            objects.remove(word)
            print("Congratulations! You remembered", word)
        else:
            print("Sorry, that was not one of the words")
        if not len(objects):
            print("You did it! You remembered all the objects")
            break
        word = input("What is your next guess? ").lower()


# play_game()
